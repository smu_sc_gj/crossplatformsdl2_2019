#include "pch.h"
#include "SDLGame.h"


SDLGame::SDLGame(){
}


SDLGame::~SDLGame(){
}


void SDLGame::initialise() {

	const int SDL_OKAY = 0;

	int sdlStatus = SDL_Init(SDL_INIT_EVERYTHING);

	if (sdlStatus != SDL_OKAY)
		throw "SDL init error";

	gameWindow = SDL_CreateWindow(
		"SDL Demo",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		800,
		600,
		SDL_WINDOW_SHOWN);

	// Use first (Default) renderer - this is usually Direct3D based
	gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);
}

void SDLGame::runGameLoop() {

	gameRunning = true;

	while (gameRunning) {

		handleEvents();
		update();
		draw();
	}
}

void SDLGame::handleEvents() {

	SDL_Event event;

	// Check for next event
	SDL_PollEvent(&event);

	switch (event.type) {

	// Check if window closed
	case SDL_QUIT:

		gameRunning = false;
		break;

	// Key pressed event
	case SDL_KEYDOWN:

		// Toggle key states based on key pressed
		switch (event.key.keysym.sym) {

		case SDLK_ESCAPE:

			gameRunning = false;
			break;
		}
		break;
	}
}

void SDLGame::update() {

}

void SDLGame::draw() {

	// Clear the screen
	SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255); // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
	SDL_RenderClear(gameRenderer);

	// Present the current frame to the screen
	SDL_RenderPresent(gameRenderer);
}
