#pragma once
class SDLGame {

private:

	bool gameRunning;

	// The game object will own the window and renderer objects
	SDL_Window*		gameWindow = nullptr;
	SDL_Renderer*	gameRenderer = nullptr;

	// Private API
	void handleEvents();
	void update();
	void draw();

public:

	SDLGame();
	~SDLGame();

	void initialise();
	void runGameLoop();

};

