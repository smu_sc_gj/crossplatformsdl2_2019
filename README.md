# Cross Platform SDL2 #

Based on year one Game Systems Fundamentals (CIS400x 2019-2020) this is one of the base project. 

Using [this](https://github.com/aminosbh/sdl2-cmake-modules) for SDL2 integration. 

## How to Clone ##

Note: Make sure you use this rather than just cloning as there's a submodule in the repository for the CMake stuff. 

```git clone --recurse-submodules https://<USERNAME>@bitbucket.org/smu_sc_gj/crossplatformsdl2_2019.git```

## Guides ##

Please see the pdf guides in the documentation folder.


